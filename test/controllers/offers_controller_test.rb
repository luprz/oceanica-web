require 'test_helper'

class OffersControllerTest < ActionController::TestCase
  setup do
    @offer = offers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:offers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create offer" do
    assert_difference('Offer.count') do
      post :create, offer: { budget_price: @offer.budget_price, code: @offer.code, days: @offer.days, description: @offer.description, night: @offer.night, package_id: @offer.package_id, standard_price: @offer.standard_price, superior_price: @offer.superior_price, title: @offer.title }
    end

    assert_redirected_to offer_path(assigns(:offer))
  end

  test "should show offer" do
    get :show, id: @offer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @offer
    assert_response :success
  end

  test "should update offer" do
    patch :update, id: @offer, offer: { budget_price: @offer.budget_price, code: @offer.code, days: @offer.days, description: @offer.description, night: @offer.night, package_id: @offer.package_id, standard_price: @offer.standard_price, superior_price: @offer.superior_price, title: @offer.title }
    assert_redirected_to offer_path(assigns(:offer))
  end

  test "should destroy offer" do
    assert_difference('Offer.count', -1) do
      delete :destroy, id: @offer
    end

    assert_redirected_to offers_path
  end
end

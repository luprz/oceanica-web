Rails.application.routes.draw do

  root to: 'frontend#index'

  get 'discover', to: 'frontend#discover' 

  get 'diving', to: 'frontend#diving' 

  get 'fishing', to: 'frontend#fishing'

  get 'kiteboarding', to: 'frontend#kiteboarding'

  get ':section/reservation/:code/:accomodation', to: 'frontend#reservation'

  post 'reserve', to: 'reservations#create' 

  get 'price_activity/:name', to: 'reservations#price_activity' 

  devise_for :users, skip: KepplerConfiguration.skip_module_devise

  mount KepplerCatalogs::Engine, :at => '/', as: 'catalogs'

  resources :admin, only: :index

  scope :admin do   

    resources :rooms do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end 

  	resources :users do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :quotes do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :activities do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :packages do 
      resources :offers do
        get '(page/:page)', action: :index, on: :collection, as: ''
        delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      end
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end 

    resources :reservations, except: :create do 
      get '/charge', to: 'reservations#create_charge'
      get '/cancel', to: 'reservations#cancel_charge'
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end   

    resources :customers do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end 
  end


  #errors
  match '/403', to: 'errors#not_authorized', via: :all, as: :not_authorized
  match '/404', to: 'errors#not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all


  #dashboard
  mount KepplerGaDashboard::Engine, :at => '/', as: 'dashboard'


end

app.directive 'customersNumberInput', ->
	{
		restrict: 'EA'
		template: '<input type="number" name="{{inputName}}" ng-model="inputValue" min=1 max=7 required/>'
		scope:
			inputValue: '='
			inputName: '='
		link: (scope) ->
			scope.$watch 'inputValue', (newValue, oldValue) ->
				arr = String(newValue).split('')
				if arr.length == 0
					return
				if arr.length == 1 and (arr[0] == '-' or arr[0] == '.')
					return
				if arr.length == 2 and newValue == '-.'
					return
				if isNaN(newValue)
					scope.inputValue = oldValue
				return
			return

	}


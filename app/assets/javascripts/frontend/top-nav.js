$(document).ready(function(){	

	var num = parseInt($('#home').css('height').split('px')[0]) - 10;

	$(window).bind('scroll', function() {
	  if ($(window).scrollTop() > num) {
	    $('#button-up a').css("display", "block")
	  } else {
	    $('#button-up a').css("display", "none")
	  }
	});

	function scrollToAnchor(aid, id){
		var aTag = $("#"+id);
		$('html,body').animate({scrollTop: 0},'slow');
	}

	$(".up_home").on('click', function() {
		scrollToAnchor(this.id, "button-up");
	});
});
class ReservationMailer < ActionMailer::Base
  default from: "gbrlmrllo@gmail.com"

  def reserve(reservation)
  	@reservation = Reservation.find(reservation.id)
    mail(to: @reservation.email, subject: "Oceanica Los Roques - Reservation Ticket(#{@reservation.status})")
  end
end
#Generado por keppler
require 'elasticsearch/model'
class Offer < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :package
  validate :characters_in_description  
  has_many :reservations, :dependent => :destroy

  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      code:  self.code.to_s,
      title:  self.title.to_s,
      description:  self.description.to_s,
      superior_price:  self.superior_price.to_s,
      standard_price:  self.standard_price.to_s,
      budget_price:  self.budget_price.to_s,
      days:  self.days.to_s,
      night:  self.night.to_s,
      package:  self.package.to_s,
    }.as_json
  end

  private
    def characters_in_description
      if self.description and self.description.size > 150
        errors.add(:description, "La descripción tiene #{self.description.size} caracteres y excede el limite de 150 caracteres.")
      end
    end

end
#Offer.import

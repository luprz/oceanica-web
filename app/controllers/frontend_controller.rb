class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
	
  def index
  	@sections = [
  		{selector: "one", name: "Discover Los Roques", url:"discover"},
  		{selector: "three", name: "Scuba Diving", url:"diving"},
  		{selector: "four", name: "Fishing", url:"fishing"},
  		{selector: "five", name: "Kiteboarding", url:"kiteboarding"}
  	]
  end

  def discover
    @quotes = Quote.all
    @rooms = Room.all
    get_gallery("Discover")
  end

  def diving
   get_gallery("Diving")
   get_offers("Diving")
  end

  def fishing
    get_gallery("Fishing")
    get_offers("Fishing")
  end

  def kiteboarding
    get_gallery("Kiteboarding")
    get_offers("Kiteboarding")
  end

  def reservation    
    @offer = Offer.find_by_code(params[:code])
    @activities = Activity.all
  end

  private
    def get_gallery(section)
      @gallery = KepplerCatalogs::Catalog.find_by_name(section).attachments.where(public:true)
    end
    
    def get_offers(section)
      @section = section
      @offers = Package.find_by_title(section).offers
    end
end

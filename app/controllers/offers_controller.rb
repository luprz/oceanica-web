#Generado con Keppler.
class OffersController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_offer, only: [:show, :edit, :update, :destroy]
  before_action :set_package

  # GET /offers
  def index
    offers = Offer.searching(@query).where({package_id: params[:package_id]}).all
    @objects, @total = offers.page(@current_page), offers.size
    redirect_to package_offers_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /offers/1
  def show
  end

  # GET /offers/new
  def new
    @offer = Offer.new
  end

  # GET /offers/1/edit
  def edit
  end

  # POST /offers
  def create
    @offer = Offer.new(offer_params)
    @offer.package_id = params[:package_id]
    if @offer.save
      redirect_to package_offers_path, notice: 'Offer was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /offers/1
  def update
    if @offer.update(offer_params)
      redirect_to package_offers_path, notice: 'Offer was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /offers/1
  def destroy
    @offer.destroy
    redirect_to package_offers_url, notice: 'Offer was successfully destroyed.'
  end

  def destroy_multiple
    Offer.destroy redefine_ids(params[:multiple_ids])
    redirect_to package_offers_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offer
      @offer = Offer.find(params[:id])
    end

    def set_package
      @package = Package.find_by_id(params[:package_id]).nil? ? redirect_to("/404") : Package.find(params[:package_id])
    end

    # Only allow a trusted parameter "white list" through.
    def offer_params
      params.require(:offer).permit(:code, :title, :description, :superior_price, :standard_price, :budget_price, :days, :night, :package_id)
    end
end
